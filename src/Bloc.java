import java.util.ArrayList;
import java.util.Iterator;

public class Bloc extends Objet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient Iterator<Objet> iter;
	private int type;
	private boolean poussable;
	private int direction ;


	public final int POUSSABLE = 1, CASSABLE = 2,FLECHE = 3;
	public final int  BAS = 2 ,GAUCHE= 4, DROITE= 6,HAUT =8;


	public Bloc(){
		super();
	}

	public Bloc(int x, int y,int type,boolean poussable,int direction){

		super(x,y);
		this.type= type;
		this.poussable = poussable;
		this.direction = direction;
	}

	public boolean checkBlock(int x, int y, ArrayList<Objet> liste){

		iter= liste.iterator();
		while(iter.hasNext()){
			Objet elem = iter.next();

			if(elem instanceof Bloc){

				if(elem.getX() == x && elem.getY() == y){
					return false;	
				}


			}
			if(elem instanceof Ramassable){

				if(elem.getX() == x && elem.getY() == y){
					return false;	
				}


			}


		}

		if(x<0) return false;
		if(x>19)return false;
		if(y<0)return false;
		if(y>9)return false;

		return true;

	}



	public void pousserBloc(Snoopy snoop,ArrayList<Objet> liste){

		if(snoop.getX() == this.getX() && snoop.getY()== this.getY()){

			if(this.getType() == POUSSABLE && this.poussable){
				switch(snoop.getDirection()){

				case HAUT: 
					if(this.checkBlock(this.getX(), this.getY()-1, liste)){
						this.moveUp();
						this.setPoussable(false);	
					}
					else snoop.moveDown();
					break;

				case BAS: 
					if(this.checkBlock(this.getX(), this.getY()+1, liste)){

						this.moveDown();
						this.setPoussable(false);	
					}
					else  snoop.moveUp();
					break;

				case DROITE:

					if(this.checkBlock(this.getX()+1, this.getY(), liste)){
						this.moveRight();
						this.setPoussable(false);	
					}
					else snoop.moveLeft();
					break;

				case GAUCHE:

					if(this.checkBlock(this.getX()-1, this.getY(), liste)){
						this.moveLeft();
						this.setPoussable(false);	
					}
					else snoop.moveRight();
					break;

				default:
					break;

				}

			}
			/// si le type est fleche on bouge snoopy selon le sens de la fleche

			else if(this.getType() == FLECHE){

				switch(this.getDirection()){

				case HAUT : 
					snoop.moveUp();
					snoop.setDirection(HAUT);
					break;
				case BAS : 
					snoop.moveDown();
					snoop.setDirection(BAS);
					break;
				case DROITE : 
					snoop.moveRight();
					snoop.setDirection(DROITE);
					break;

				case GAUCHE : 
					snoop.moveLeft();
					snoop.setDirection(GAUCHE);
					break;
				default:
					break;

				}

			}

			else {
				switch(snoop.getDirection()){

				case HAUT: 
					snoop.moveDown();
					break;

				case BAS: 
					snoop.moveUp();
					break;

				case DROITE:
					snoop.moveLeft();
					break;

				case GAUCHE:
					snoop.moveRight();
					break;

				default:
					break;

				}


			}
		}

	}

	public boolean casserBloc (Snoopy snoop){

		switch(snoop.getDirection()){

		case DROITE :
			if(snoop.getX() == this.getX()-1 && snoop.getY()== this.getY()){


				return true;

			}
			break;
		case BAS :
			if(snoop.getX() == this.getX() && snoop.getY()== this.getY()-1){
				return true;
			}
			break;
		case GAUCHE :
			if(snoop.getX() == this.getX()+1 && snoop.getY()== this.getY()){
				return true;

			}
			break;
		case HAUT :
			if (snoop.getX() == this.getX() && snoop.getY()== this.getY()+1){
				return true;
			}
			break;
		}
		return false;
	}
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isPoussable() {
		return poussable;
	}

	public void setPoussable(boolean poussable) {
		this.poussable = poussable;
	}


	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "Bloc [type=" + type +  ", poussable=" + poussable + ", toString()="
				+ super.toString() + "]";
	}





}
