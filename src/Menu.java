import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Menu extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JButton jb1,jb2,jb3,jb4,jb5,jb6,jb7,jb8;
	public static Menu m;
	public static Fichier fichier;

	public  static Jeu j ;
	public static Editeur ed;
	final JPanel pan = new JPanel();
	private JFrame dialogs;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		m = new Menu();


	}

	public Menu(){

		super("Menu");
		this.setSize(300, 300);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setContentPane(pan);
		this.setLocationRelativeTo(null);

		pan.setLayout(new BoxLayout(pan, BoxLayout.Y_AXIS));
		pan.setBackground(Color.BLACK); 

		jb1 = new JButton("Jouer");
		jb2 = new JButton("Score");
		jb3 = new JButton("Mot de Passe");
		jb4 = new JButton("Charger");
		jb5 = new JButton("Quitter");
		jb7 = new JButton("Editeur");

		jb1.setAlignmentX(Component.CENTER_ALIGNMENT);
		jb1.setMaximumSize(getMaximumSize());
		jb1.addActionListener(this);
		jb2.setAlignmentX(Component.CENTER_ALIGNMENT);
		jb2.addActionListener(this);
		jb2.setMaximumSize(getMaximumSize());
		jb3.setAlignmentX(Component.CENTER_ALIGNMENT);
		jb3.addActionListener(this);
		jb3.setMaximumSize(getMaximumSize());
		jb4.setAlignmentX(Component.CENTER_ALIGNMENT);
		jb4.addActionListener(this);
		jb4.setMaximumSize(getMaximumSize());
		jb5.setAlignmentX(Component.CENTER_ALIGNMENT);
		jb5.setMaximumSize(getMaximumSize());
		jb5.addActionListener(this);
		
		jb7.setAlignmentX(Component.CENTER_ALIGNMENT);
		jb7.setMaximumSize(getMaximumSize());
		jb7.addActionListener(this);

		pan.add(jb1);
		pan.add(jb2);
		pan.add(jb3);
		pan.add(jb4);
		pan.add(jb7);
		pan.add(jb5);
		
		

		fichier = new Fichier();
		this.setVisible(true);
	}

	public Menu(int num){

		j= new Jeu(num);
		jb6 = new JButton("Sauvegarder");

		this.setTitle("Game");
		this.setSize(1100, 600);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setLocationRelativeTo(null);
		this.setLocation(50, 50);
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);

		this.getContentPane().add(j);
		jb6.addActionListener(this);
		this.getContentPane().add(jb6, BorderLayout.EAST);

		this.setVisible(true);		


	}
	
	public Menu(Editeur e){
		

		jb8 = new JButton("Sauvegarder");
		
		this.setTitle("Editeur");
		this.setSize(1100, 600);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setLocationRelativeTo(null);
		this.setLocation(50, 50);
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);
		
		this.getContentPane().add(e);
		jb8.addActionListener(this);
		this.getContentPane().add(jb8, BorderLayout.EAST);

		this.setVisible(true);		
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		Object source = e.getSource();

		// jouer 
		

		if(source == jb1){

			int num=1;

			m.dispose();
			m = new Menu(num);

		}


		//afficher les scores

		if(source == jb2){

			m.dispose();
			JOptionPane.showMessageDialog(m, fichier.afficherScore(), "Score" , JOptionPane.PLAIN_MESSAGE);
			m = new Menu();
			
			
			
			//j.fichier.afficherScore();

		}

		//mot de passe
		if(source == jb3){

			m.dispose();


			// les textes figurant sur les boutons
			String lesTextes[]={ "LVL 1",    "LVL 2",
			"LVL 3"};
			// indice du bouton qui a été cliqué ou CLOSED_OPTION
			int showOptionDialog = JOptionPane.showOptionDialog(dialogs, "Choix du niveau", "Choix",  JOptionPane.YES_NO_OPTION, 
					JOptionPane.QUESTION_MESSAGE, null, lesTextes,lesTextes[0]);
			int  lvl  =  showOptionDialog;  // le bouton par défaut
			if( lvl!=JOptionPane.CLOSED_OPTION) ;// un bouton cliqué
			else  m= new Menu();        

			lvl++;

			
			///CHARGEMENT DU MOT PASSE  POUR LE NIVEAU 1

			if(lvl == 1) {

				String mdp = "lvl1";
				String mdpr = null;
				mdpr =  JOptionPane.showInputDialog(dialogs, "Mot de Passe :");

				if(mdpr.compareTo(mdp)== 0){

					m = new Menu(lvl);
				}
				else{
					JOptionPane.showMessageDialog(dialogs,"Sorry bro");
					m= new Menu();
				}
			}
			
			///CHARGEMENT DU MOT PASSE  POUR LE NIVEAU 2

			if(lvl == 2) {

				String mdp = "lvl2";
				String mdpr = null;

				mdpr =  JOptionPane.showInputDialog(dialogs, "Mot de Passe :");

				if(mdpr.compareTo(mdp)== 0){
					m = new Menu(lvl);
				}
				else{
					JOptionPane.showMessageDialog(dialogs,"Sorry bro");
					m= new Menu();
				}


			}


			///CHARGEMENT DU MOT PASSE  POUR LE NIVEAU 3
			
			if(lvl == 3) {

				String mdp = "lvl3";
				String mdpr = null;

				mdpr =  JOptionPane.showInputDialog(dialogs, "Mot de Passe :");

				if(mdpr.compareTo(mdp)== 0){
					m = new Menu(lvl);
				}
				else{
					JOptionPane.showMessageDialog(dialogs,"Sorry bro");
					m= new Menu();
				}


			}
		}

		//CHARGER UNE PARTIE

		if(source == jb4){

			m.dispose();
			m= new Menu(0);

		}

		//QUITTER
		if(source == jb5){

			this.dispose();	
			System.exit(0);
		}

		// SAUVERGARDER DANS LE JEU
		if(source == jb6)
		{
			j.pause = true;
			j.snoop.setTimer(j.getTime1());
			j.snoop.setLvl(Jeu.lvl);
			
			j.sauvegarderListe();
			j.sauvergarderScore();

			this.dispose();
			m = new Menu();

		}
		
		if(source == jb7)
		{
			m.dispose();
		    ed = new Editeur();
			m = new Menu(ed);

		}
		if(source == jb8)
		{
			
			
			ed.sauvegarder();
			
			//fichier.SaveListe(Editeur.liste);
			
			this.dispose();
			m = new Menu();

		}
		
		
		
		
		

	}

}
