import java.util.ArrayList;
import java.util.Iterator;

public class Snoopy extends Objet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int vie, oiseaux, direction,timer,lvl,score;

	private transient Iterator<Objet> iter;

	public final int OISEAU = 1,PIEGE = 2;
	public final int POUSSABLE = 1, CASSABLE = 2, FLECHE = 3;
	public final int  BAS = 2 ,GAUCHE= 4, DROITE= 6,HAUT =8;


	public Snoopy(int x, int y, int vie, int oiseaux) {	
		super(x,y);
		this.vie = vie;
		this.oiseaux = oiseaux;
		this.direction = BAS;
		this.timer=60;	
		this.lvl = 1;
		this.score = 0;

	}


	public boolean checkLife(){
		if(this.getVie() <= 0) return true;
		else return false;
	}
	
	
	public int getLvl() {
		return lvl;
	}

	public void setLvl(int lvl) {
		this.lvl = lvl;
	}

	public boolean checkOiseau(){
		if( this.getOiseaux()== 4)return true;
		else return false;
	}

	public boolean checkMove(ArrayList<Objet> liste)
	{
		iter = liste.iterator();

		while(iter.hasNext()){
			Objet elem = iter.next();

			if(elem instanceof Bloc){

				if(elem.getX()== this.getX() && elem.getY() == this.getY()) return false;
			
			}	
			


			if(elem instanceof Ramassable){

				if(((Ramassable) elem).getType() == OISEAU ){
					if(elem.getX()== this.getX() && elem.getY() == this.getY()){
						this.setOiseaux(this.getOiseaux()+1);
						iter.remove();
					}
				}
					
				else if(((Ramassable) elem).getType() == PIEGE ){
						if(elem.getX()== this.getX() && elem.getY() == this.getY()){
							this.lostLife();
							iter.remove();
						}
				
					}
			

			}
		}


		return true;
	}

	public void move(ArrayList<Objet> liste){

		if( this.checkMove(liste))
		{
			switch(this.getDirection()){

			case BAS :
				this.moveDown();
				break;

			case HAUT : 
				this.moveUp();
				break;
			case GAUCHE:
				this.moveLeft();
				break;
			case DROITE:
				this.moveRight();
				break;	
			}

		}

	}

	public void lostLife(){
		this.setVie(this.getVie()-1);
	}

	public void moveRight(){

		this.setX(this.getX()+1);

	}
	public void moveLeft(){
		this.setX(this.getX()-1);
	}

	public void moveUp(){
		this.setY(this.getY()-1);
	}

	public void moveDown(){
		this.setY(this.getY()+1);
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}


	public int getVie() {
		return vie;
	}
	public void setVie(int vie) {
		this.vie = vie;
	}
	public int getOiseaux() {
		return oiseaux;
	}
	public void setOiseaux(int oiseaux) {
		this.oiseaux = oiseaux;
	}

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}
	

	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	@Override
	public String toString() {
		return "Snoopy [vie=" + vie + ", oiseaux=" + oiseaux + ", direction=" + direction + ", timer=" + timer
				+ ", toString()=" + super.toString() + "]";
	}







}
