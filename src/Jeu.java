import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Jeu extends Canvas implements KeyListener,ActionListener, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final int LARGEUR = 10, LONGUEUR = 20;
	public final int POUSSABLE = 1,CASSABLE= 2, FLECHE = 3;
	public final int OISEAU = 1, PIEGE = 2;

	public final int  BAS = 2 ,GAUCHE= 4, DROITE= 6,HAUT =8;


	private Timer time; 
	public ArrayList<Objet> liste ;
	private transient Iterator<Objet> iter ;

	// chargement des images
	private transient Image imgDroite = Toolkit.getDefaultToolkit().getImage("img/SnoopDroite.png");
	private transient Image imgGauche =Toolkit.getDefaultToolkit().getImage("img/SnoopGauche.png");
	private transient Image imgHaut = Toolkit.getDefaultToolkit().getImage("img/SnoopHaut.png");
	private transient Image imgBas =Toolkit.getDefaultToolkit().getImage("img/SnoopBas.png");
	private transient Image imgBlocpoussable =Toolkit.getDefaultToolkit().getImage("img/BlocDepl.png");
	private transient Image imgBlocCassable =Toolkit.getDefaultToolkit().getImage("img/BlocCassable.png");
	private transient Image imgFlecheUp  = Toolkit.getDefaultToolkit().getImage("img/FlecheHaut.png");
	private transient Image imgFlecheDown = Toolkit.getDefaultToolkit().getImage("img/FlecheBas.png");
	private transient Image imgFlecheLeft =Toolkit.getDefaultToolkit().getImage("img/FLecheGauche.png");
	private transient Image imgFlecheRight =Toolkit.getDefaultToolkit().getImage("img/FlecheDroite.png");
	private transient Image imgOiseau = Toolkit.getDefaultToolkit().getImage("img/oiseau.png");
	private transient Image imgBombe = Toolkit.getDefaultToolkit().getImage("img/bombe.png");
	private transient Image imgCoeur = Toolkit.getDefaultToolkit().getImage("img/coeur.png");


	public Snoopy  snoop;
	
	public static int lvl;
	public boolean pause=false;
	
	private JFrame dialogs ;
	


	private boolean entrepause = false, win = false,loose ,looseL;
	private long chrono=0, chronoInit=0, chronoPause=0,chronoPause1 =0;
	private int time1=60 , score = 0 , score2 = 0;
	private String namePlayer;

	public Jeu(){
		
	}


	public Jeu(int num){

		this.addKeyListener(this);


		liste = new ArrayList<Objet>();
		
		namePlayer = JOptionPane.showInputDialog(dialogs, "Nom du player :");
		lvl = num;

		
		if(num > 0 && num <4){
			liste = Menu.fichier.loadLevel(num);
			this.createSnoop(liste);
		}
		if(num == 0){
			liste = Menu.fichier.chargerPartie();
			this.createSnoop(liste);
			lvl= snoop.getLvl();
			score = snoop.getScore();
		}
		
		time = new Timer(400,this);
		chronoInit = System.currentTimeMillis();
		time.start();

	}



	public void createSnoop(ArrayList<Objet> li){

		iter = li.iterator();

		while(iter.hasNext()){

			Objet elem = iter.next();

			if(elem instanceof Snoopy){
				snoop = (Snoopy) elem;
			}
		}

	}
	
	
	public void addNewBalle(int x, int y , boolean dx, boolean dy){
		Balle balle = new Balle(x,y,dx,dy);
		liste.add(balle);
	}

	public void addNewBloc(int x, int y , int type , boolean poussable, int direction){

		Bloc b = new Bloc(x,y,type,poussable,direction);
		liste.add(b);	
	}

	public void addRamassable(int x, int y , int type){

		Ramassable r = new Ramassable(x,y,type);
		liste.add(r);
	}

	public void sauvegarderListe(){

		Menu.fichier.SaveListe(liste);
	}

	public void sauvergarderScore(){

		String enr = namePlayer +" "+score2+"\n";
		Menu.fichier.enregistrerScore(enr);
		//fichier.enregistrerScore(namePlayer, score2);
	}

	public void paint(Graphics g){

		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 1000, 600);

		//INITIALISER LA GRILLE EN GRAPHIQUE
		g.setColor(Color.BLACK);

		for(int i = 0; i<=LONGUEUR;i++)
		{
			for( int j= 0;j<LARGEUR +1 ; j++)
			{				
				g.drawLine(45,(j+1)*45 , (LONGUEUR+1)*45, (j+1)*45);			
			}
			g.drawLine((i+1)*45, 45 , (i+1)*45,(LARGEUR+1)*45);
		}


		//position 

		g.setColor(Color.BLACK);
		g.drawString("Nom du joueur :"+namePlayer, 5, 20);
		g.drawString("lvl"+lvl , 250, 20);
		
		g.drawString("Score "+score2 , 650, 20);


		if(pause)g.drawString("PAUSE ACTIVE ",520 , 20);
		else g.drawString("PAUSE DESACTIVE ",520 , 20);


		// affichage du score 

		g.drawString(""+time1, 800, 20);
		g.drawRect(850, 10, 60, 10);
		g.fillRect(850, 10, time1, 10);

		iter=liste.iterator();

		while(iter.hasNext()){
			Objet elem = iter.next();


			if(elem instanceof Snoopy){
				g.setColor(Color.BLACK);

				if(snoop.getVie() >0) {
					g.drawImage(imgCoeur,300, 10 , 20, 20, this);
					if(snoop.getVie() >1){
						g.drawImage(imgCoeur,330, 10 , 20, 20, this);
						if(snoop.getVie() >2){
							g.drawImage(imgCoeur,360, 10 , 20, 20, this);
						}
					}
				}
				

				if(snoop.getOiseaux() >0) {
					g.drawImage(imgOiseau,400, 10 , 20, 20, this);
					if(snoop.getOiseaux() >1){
						g.drawImage(imgOiseau,430, 10 , 20, 20, this);
						if(snoop.getOiseaux() >2){
							g.drawImage(imgOiseau,460, 10 , 20, 20, this);
							if(snoop.getOiseaux() >3){
								g.drawImage(imgOiseau,490, 10 , 20, 20, this);
							}
						}
					}
				}
				


				if(((Snoopy) elem).getDirection()== DROITE) g.drawImage(imgDroite,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Snoopy) elem).getDirection()== GAUCHE) g.drawImage(imgGauche,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Snoopy) elem).getDirection()== HAUT) g.drawImage(imgHaut,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Snoopy) elem).getDirection()== BAS) g.drawImage(imgBas,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);

			}

			else if(elem instanceof Bloc){

				if(((Bloc) elem).getType()== CASSABLE)g.drawImage(imgBlocCassable,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Bloc) elem).getType()== POUSSABLE) g.drawImage(imgBlocpoussable,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);

				else if(((Bloc)elem).getType() == FLECHE){

					switch(((Bloc)elem).getDirection()){

					case HAUT: g.drawImage(imgFlecheUp,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;	

					case BAS:g.drawImage(imgFlecheDown,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;

					case DROITE:g.drawImage(imgFlecheRight,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;

					case GAUCHE:g.drawImage(imgFlecheLeft,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;

					default:
						break;
					}
				}

			}

			else if(elem instanceof Balle){

				g.setColor(Color.BLUE);
				g.fillOval( (elem.getX()+1) *45 + 15, (elem.getY()+1)*45 +15, 20, 20);

			}

			else if(elem instanceof Ramassable){

				if(((Ramassable) elem).getType() == OISEAU ) g.drawImage(imgOiseau,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Ramassable) elem).getType()== PIEGE) g.drawImage(imgBombe,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
			}

		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub


		if(e.getKeyCode()==37){ 
			if(snoop.getX()>0 && !pause){

				this.moveSnoop(GAUCHE);
				repaint();
			}

		}

		if(e.getKeyCode()==39){ 
			if(snoop.getX()<19 && !pause)
			{

				this.moveSnoop(DROITE);
				repaint();
			}
		}

		if(e.getKeyCode()==40){ 
			if(snoop.getY()<9 && !pause)
			{
				this.moveSnoop(BAS);

				repaint();
			}
		}

		if(e.getKeyCode()==38){  
			if(snoop.getY()>0 && !pause){

				this.moveSnoop(HAUT);


				repaint();
			}
		}

		if(e.getKeyCode()==65){  // touche A POUR CASSER LES BLOCS
			if(!pause){



				iter= liste.iterator();

				while(iter.hasNext()){
					Objet elem = iter.next();
					if(elem instanceof Bloc){


						if(((Bloc) elem).getType()==CASSABLE){

							if( ( (Bloc) elem).casserBloc(snoop)){
								iter.remove();


							}
						}
					}
				}
			}
		}


		if(e.getKeyCode() == 80)   //TOUCHE P POUR LA PAUSE 
		{
			pause = ! pause;

			if (pause){
				chronoPause = 0;
				chronoPause = (int) System.currentTimeMillis();
				entrepause = true;
			}
			if(entrepause && !pause){

				chronoPause -= (int) System.currentTimeMillis();				
				chronoPause = -chronoPause /1000;	
				chronoPause1 += chronoPause;

				entrepause = false;
			}


		}


	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub


		// quand il n y as pas de pause 
		if(!pause){

			time.setDelay(250);
			iter= liste.iterator();


			while(iter.hasNext()){
				Objet elem = iter.next();

				if(elem instanceof Snoopy){

					//loose = ((Snoopy)elem).checkTemps(time1);
					looseL = ((Snoopy)elem).checkLife();

				  win = ((Snoopy)elem).checkOiseau();


				}

				if(elem instanceof Bloc){

					((Bloc) elem).pousserBloc(snoop,liste);	

				}

				if(elem instanceof Balle)
				{
					((Balle) elem).checkAll(snoop,liste);
				}

			}



			chrono = (System.currentTimeMillis() - chronoInit )/1000;

			repaint();	
		}
		

		time1 = (int) ( snoop.getTimer() - chrono +chronoPause1 );
		loose= checkTime();
		score = time1 * 100;
		score2 = snoop.getScore()+ score;

		 // si on a perdu toute notre vie
		
		if(looseL){
			
			//score2= snoop.getScore();
			JOptionPane.showMessageDialog(dialogs, "Plus de vie recommence depuis le début");
			liste = Menu.fichier.loadLevel(/*lvl*/1);
			lvl=1;
			this.createSnoop(liste);
			//snoop.setScore(score2);
			chronoInit = System.currentTimeMillis();

		}
		// si le temps est écoulé
		if(loose){

			score2= snoop.getScore();
			JOptionPane.showMessageDialog(dialogs, "Plus de temps, recommence le niveau");
			liste = Menu.fichier.loadLevel(lvl);
			this.createSnoop(liste);
			snoop.setScore(score2);
			chronoInit = System.currentTimeMillis();

		}

	
		if(win){

			JOptionPane.showMessageDialog(dialogs, " WINS THIS LVL "+lvl);
			win=false;

			
			snoop.setLvl(lvl);
			snoop.setScore(score2);
			lvl++;

			if(lvl > 1 && lvl < 4){
				
				liste = Menu.fichier.loadLevel(lvl);
				this.createSnoop(liste);
				snoop.setScore(score2);
			
				chronoInit = System.currentTimeMillis();
				
			}
			else {
				
				this.invalidate();
				Menu.m.dispose();
				this.sauvergarderScore();
				Menu.m = new Menu();
				time.stop();
				 
				
			}
		}

		repaint();



	}
	
	public boolean checkTime(){
		if(this.time1 <1)return true;
		else return false;
		
	}
	
	public void moveSnoop(int direction){

		snoop.setDirection(direction);
		snoop.move(liste);

	}


	public int getTime1() {
		return time1;
	}


	public void setTime1(int time1) {
		this.time1 = time1;
	}

	public String getNamePlayer() {
		return namePlayer;
	}

	public void setNamePlayer(String name) {
		this.namePlayer = name;
	}

	public boolean isLoose() {
		return loose;
	}

	public void setLoose(boolean loose) {
		this.loose = loose;
	}








}
