import java.io.*;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;



public class Fichier extends ArrayList<Object> implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JFrame dialogs;

	private ArrayList<Objet> liste;

	public Fichier(){

	}
	public void enregistrerScore(String enr){

		File f = new File ("Score");

		try
		{
			FileWriter fw = new FileWriter (f,true);
			BufferedWriter bw = new  BufferedWriter(fw);
			bw.write(enr);
			bw.close();
			fw.close();
		}
		catch (IOException exception)
		{
			System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
		}
	}



	public String afficherScore(){

		File f = new File ("Score");
		String rt ="";
		try
		{
			FileReader fr = new FileReader (f);
			BufferedReader br = new BufferedReader (fr);


			try
			{

				String line = br.readLine();

				while (line != null)
				{
					rt += line+"\n";
					line = br.readLine();
				}

				br.close();
				fr.close();
			}
			catch (IOException exception)
			{
				System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
			}
		}
		catch (FileNotFoundException exception)
		{
			System.out.println ("Le fichier n'a pas été trouvé");
		}

		return rt;


	}

	@SuppressWarnings("unchecked")
	public ArrayList<Objet> loadLevel(int numLvl){

		//Declarations
		ArrayList<Objet> liste = new ArrayList<Objet>();
		String lvlFileName = null;

		//Recuperation du nom du ficher a charger
		switch(numLvl){
		case 1:
			lvlFileName="lvl1";
			break;
		case 2:
			lvlFileName="lvl2";
			break;
		case 3:
			lvlFileName="lvl3";
			break;
		default:
			System.out.println("Une erreur est survenue dans le chargement du niveau !");
		}

		try {
			//Ouverture du fichier a charger
			FileInputStream saveFile = new FileInputStream(lvlFileName);
			ObjectInputStream save = new ObjectInputStream(saveFile);

			//Chargement des donnees
			liste = ((ArrayList<Objet>) save.readObject());

			//Fermeture du fichier
			save.close();
			saveFile.close();

		} catch (IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Une erreur est survenue pendant le chargement !");

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return liste ;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Objet> chargerPartie(){

		String save_filename = null;

		//Recuperation du nom de fichier de sauvegarde
		save_filename = JOptionPane.showInputDialog(dialogs, "Nom du fichier de sauvegarde :");


		try {
			//Ouverture du fichier a charger
			FileInputStream saveFile = new FileInputStream(save_filename);
			ObjectInputStream save = new ObjectInputStream(saveFile);

			//Chargement des donnees
			liste = ((ArrayList<Objet>) save.readObject());

			//Fermeture du fichier
			save.close();
			saveFile.close();

		} catch (IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Une erreur est survenue pendant le chargement !");
			Menu.m=new Menu();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Pas de fichier pour ce nom !");
		}

		return liste;
	}

	public void SaveListe(ArrayList<Objet> liste){
		//Declarations
		String save_filename = "";

		//Recuperation du nom de fichier de sauvegarde
		save_filename = JOptionPane.showInputDialog(dialogs, "Nom du fichier de sauvegarde :");


		try {

			//Ouverture du fichier de sauvegarde
			FileOutputStream saveFile = new FileOutputStream(save_filename);
			ObjectOutputStream save = new ObjectOutputStream(saveFile);

			//Sauvegarder


			save.writeObject(liste);


			//Fermeture du ficher
			save.close();
			saveFile.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			//Gestion des exceptions
			System.out.println("Une erreur est survenue pendant la sauvegarde !");
		}


	}

	public void SaveListe(ArrayList<Objet> liste,String lvl){
	

		try {

			//Ouverture du fichier de sauvegarde
			FileOutputStream saveFile = new FileOutputStream(lvl);
			ObjectOutputStream save = new ObjectOutputStream(saveFile);

			//Sauvegarder


			save.writeObject(liste);


			//Fermeture du ficher
			save.close();
			saveFile.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			//Gestion des exceptions
			System.out.println("Une erreur est survenue pendant la sauvegarde !");
		}


	}



}
