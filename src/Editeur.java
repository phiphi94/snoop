import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Editeur extends Canvas implements KeyListener{
	
	
	
	public final int LARGEUR = 10, LONGUEUR = 20;
	public final int POUSSABLE = 1,CASSABLE= 2, FLECHE = 3;
	public final int OISEAU = 1, PIEGE = 2;
	public final int  BAS = 2 ,GAUCHE= 4, DROITE= 6,HAUT =8;
	public final int SNOOPY = 10, BCASSABLE = 11, BDEPLACABLE = 12 , BALLE = 13,OISO = 14,BPIEGE =15 ;
	public static int selection = 10, comptS = 0, comptO = 0, comptB =0 , typeF = 0;
	
	
	// chargement des images
	private transient Image imgDroite = Toolkit.getDefaultToolkit().getImage("img/SnoopDroite.png");
	private transient Image imgGauche =Toolkit.getDefaultToolkit().getImage("img/SnoopGauche.png");
	private transient Image imgHaut = Toolkit.getDefaultToolkit().getImage("img/SnoopHaut.png");
	private transient Image imgBas =Toolkit.getDefaultToolkit().getImage("img/SnoopBas.png");
	private transient Image imgBlocpoussable =Toolkit.getDefaultToolkit().getImage("img/BlocDepl.png");
	private transient Image imgBlocCassable =Toolkit.getDefaultToolkit().getImage("img/BlocCassable.png");
	private transient Image imgFlecheUp  = Toolkit.getDefaultToolkit().getImage("img/FlecheHaut.png");
	private transient Image imgFlecheDown = Toolkit.getDefaultToolkit().getImage("img/FlecheBas.png");
	private transient Image imgFlecheLeft =Toolkit.getDefaultToolkit().getImage("img/FLecheGauche.png");
	private transient Image imgFlecheRight =Toolkit.getDefaultToolkit().getImage("img/FlecheDroite.png");
	private transient Image imgOiseau = Toolkit.getDefaultToolkit().getImage("img/oiseau.png");
	private transient Image imgBombe = Toolkit.getDefaultToolkit().getImage("img/bombe.png");
	


	
	private int posx = 0,posy = 0 ;
	
	public  ArrayList<Objet> liste ;
	private static JFrame dialogs ;
	private transient Iterator<Objet> iter;


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Editeur(){
		
		this.addKeyListener(this);
		
		liste = new ArrayList<Objet>();
		
		
	}
	
	public  void sauvegarder(){
		
		boolean again = true;
		int lvl = 0;
		while(again){
			again= false;
		
		String lesTextes[]={ "Autre","LVL 1", "LVL 2",
		"LVL 3"};
		// indice du bouton qui a été cliqué ou CLOSED_OPTION
		int showOptionDialog = JOptionPane.showOptionDialog(dialogs, "Choix du niveau", "Choix",  JOptionPane.YES_NO_OPTION, 
				JOptionPane.QUESTION_MESSAGE, null, lesTextes,lesTextes[0]);
	      lvl  =  showOptionDialog;  // le bouton par défaut
		if( lvl!=JOptionPane.CLOSED_OPTION) ;// un bouton cliqué
		else again= true;
		}
		
		this.changeLvlSnoop(lvl);
		
		if(lvl == 0)Menu.fichier.SaveListe(liste);
		if(lvl == 1) Menu.fichier.SaveListe(liste,"lvl1");
		if(lvl == 2) Menu.fichier.SaveListe(liste,"lvl2");
		if(lvl == 3) Menu.fichier.SaveListe(liste,"lvl3");
		
		

	}
	
	public void changeLvlSnoop(int lvl){
		
		iter = liste.iterator();
		while(iter.hasNext())
		{
			Objet elem = iter.next();
			
			if(elem instanceof Snoopy) ((Snoopy)elem).setLvl(lvl);
		}
	}
	



	public void addSnoopy(int x, int y){
		
		Snoopy snoop = new Snoopy(x,y,3,0);
		snoop.setLvl(0);
		liste.add(snoop);
		
	}
	
	public void addNewBalle(int x, int y , boolean dx, boolean dy){
		Balle balle = new Balle(x,y,dx,dy);
		liste.add(balle);
	}

	public void addNewBloc(int x, int y , int type , boolean poussable, int direction){

		Bloc b = new Bloc(x,y,type,poussable,direction);
		liste.add(b);	
	}

	public void addRamassable(int x, int y , int type){

		Ramassable r = new Ramassable(x,y,type);
		liste.add(r);
	}
	
	public void paint(Graphics g){
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 1000, 600);

		//INITIALISER LA GRILLE EN GRAPHIQUE
		g.setColor(Color.BLACK);

		for(int i = 0; i<=LONGUEUR;i++)
		{
			for( int j= 0;j<LARGEUR +1 ; j++)
			{				
				g.drawLine(45,(j+1)*45 , (LONGUEUR+1)*45, (j+1)*45);			
			}
			g.drawLine((i+1)*45, 45 , (i+1)*45,(LARGEUR+1)*45);
		}
		
		iter=liste.iterator();

		while(iter.hasNext()){
			Objet elem = iter.next();


			if(elem instanceof Snoopy){
				g.setColor(Color.BLACK);


				if(((Snoopy) elem).getDirection()== DROITE) g.drawImage(imgDroite,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Snoopy) elem).getDirection()== GAUCHE) g.drawImage(imgGauche,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Snoopy) elem).getDirection()== HAUT) g.drawImage(imgHaut,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Snoopy) elem).getDirection()== BAS) g.drawImage(imgBas,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);

			}

			else if(elem instanceof Bloc){

				if(((Bloc) elem).getType()== CASSABLE)g.drawImage(imgBlocCassable,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Bloc) elem).getType()== POUSSABLE) g.drawImage(imgBlocpoussable,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);

				else if(((Bloc)elem).getType() == FLECHE){

					switch(((Bloc)elem).getDirection()){

					case HAUT: g.drawImage(imgFlecheUp,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;	

					case BAS:g.drawImage(imgFlecheDown,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;

					case DROITE:g.drawImage(imgFlecheRight,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;

					case GAUCHE:g.drawImage(imgFlecheLeft,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
					break;

					default:
						break;
					}
				}

			}

			else if(elem instanceof Balle){

				g.setColor(Color.BLUE);
				g.fillOval( (elem.getX()+1) *45 + 15, (elem.getY()+1)*45 +15, 20, 20);

			}

			else if(elem instanceof Ramassable){

				if(((Ramassable) elem).getType() == OISEAU ) g.drawImage(imgOiseau,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
				else if(((Ramassable) elem).getType()== PIEGE) g.drawImage(imgBombe,(elem.getX()+1)* 45 , (elem.getY()+1)* 45, 45, 45, this);
			}

		}
		
		g.drawString("Press S: Snoopy, C :Bloc Cassable, D : Bloc Deplacable, B : Balle , O : Oiseau  ", 10, 10);
		g.drawString("Press P: Piege , F : FLECHE   ", 10, 30);
		
		g.setColor(Color.BLUE);
		g.fillRect((posx +1)*45, (posy+1)*45, 45, 45);
		
		this.showSelection(g);
		
	
	}
	
	public void showSelection(Graphics g){
		
		if(selection == SNOOPY)g.drawImage(imgBas,700 , 10, 25, 25, this);
		if(selection == OISO)g.drawImage(imgOiseau,700 , 10, 25, 25, this);
		if(selection == BCASSABLE)g.drawImage(imgBlocCassable,700 , 10, 25, 25, this);
		if(selection == BDEPLACABLE)g.drawImage(imgBlocpoussable,700 , 10, 25, 25, this);
		if(selection == BPIEGE)g.drawImage(imgBombe,700 , 10, 25, 25, this);
		if(selection == BALLE )g.fillOval(700, 10, 25, 25);
		if(selection == FLECHE){
			if(typeF == BAS)g.drawImage(imgFlecheDown,700 , 10, 25, 25, this);
			if(typeF == HAUT)g.drawImage(imgFlecheUp,700 , 10, 25, 25, this);
			if(typeF == DROITE)g.drawImage(imgFlecheRight,700 , 10, 25, 25, this);
			if(typeF == GAUCHE)g.drawImage(imgFlecheLeft,700 , 10, 25, 25, this);
		}
		
	}
	
	public void afficherListe(){
		
		iter = liste.iterator();
		while(iter.hasNext()){
			System.out.println(iter.next().toString());
		}
	}
	
	
	public boolean checkAdd(){
		
		
		iter= liste.iterator();
		
		while(iter.hasNext()){
			Objet elem = iter.next();
			
			if(elem.getX() == this.posx && elem.getY() == this.posy)return false;
			
			
		}
		return true;
		
	}

	
	public void addSelection(int selection, int tf){
		
		if(this.checkAdd()){
		if(selection == SNOOPY){
			
			
			if(comptS<1){
			this.addSnoopy(posx, posy);
			comptS++;
			}
			else JOptionPane.showMessageDialog(dialogs, "Un seul mon Coco");
	
			
		}
		if(selection == OISO){
			
			if(comptO <4){
			
				this.addRamassable(posx, posy, OISEAU);
				comptO++;
				}
			else JOptionPane.showMessageDialog(dialogs, "Pas plus de 4");
				
		}
		if(selection == BALLE){
			if(comptB <2){
				this.addNewBalle(posx, posy, true, true);
				comptB++;
				System.out.println(comptS);
			}
			else JOptionPane.showMessageDialog(dialogs, "Pas plus de 2");
	
		}
		
		if(selection == BCASSABLE){
			
			this.addNewBloc(posx, posy, CASSABLE, false, 0);
			
		}
		if(selection == BDEPLACABLE){
			
			this.addNewBloc(posx, posy, POUSSABLE, true, 0);
			
		}
	   if(selection == BPIEGE){
			
		this.addRamassable(posx, posy, PIEGE);
			
		}
	   
	   if(selection == FLECHE)
	   {
		   
		   this.addNewBloc(posx, posy, FLECHE, false, tf);
	   }
		
		
		
		this.afficherListe();
		
		}else JOptionPane.showMessageDialog(dialogs, "Pas sur la même position");
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
		
		if(e.getKeyCode()==37){ 
			if(this.posx >0 ){

				posx--;  // GAUCHE
				repaint();
			}

		}

		if(e.getKeyCode()==39){ 
			if(this.posx<19)
			{

				this.posx++; // DROITE
				repaint();
			}
		}

		if(e.getKeyCode()==40){  //BAS
			if(this.posy<9 )
			{
				this.posy++;
				repaint();
			}
		}

		if(e.getKeyCode()==38){    //HAUT
			if(this.posy >0){

				this.posy--;
				repaint();
			}
		
		}
		if(e.getKeyCode()==83){ // TOUCHE S
			selection =SNOOPY;
		}
		
		if(e.getKeyCode()==67){ // TOUCHE C
			selection = BCASSABLE;
			
		}
		
		if(e.getKeyCode()==68){ // TOUCHE D
			selection = BDEPLACABLE;
		}
		
		if(e.getKeyCode()==66){ // TOUCHE B
			selection = BALLE;
		}
		
		if(e.getKeyCode()==79){ // TOUCHE O
			selection = OISO;
		}
		if(e.getKeyCode()==80){ // TOUCHE P
			
			selection = BPIEGE;
		}
       if(e.getKeyCode()==70){ // TOUCHE F
			
    	   
    	   selection = FLECHE;
    		// les textes figurant sur les boutons
			String lesTextes[]={ "FLECHE HAUT",    "FLECHE BAS",
			"FLECHE DROITE" , "FLECHE GAUCHE"};
			// indice du bouton qui a été cliqué ou CLOSED_OPTION
			int showOptionDialog = JOptionPane.showOptionDialog(dialogs, "Choisie le type de fleche", "Choix",  JOptionPane.YES_NO_OPTION, 
					JOptionPane.QUESTION_MESSAGE, null, lesTextes,lesTextes[0]);
			  typeF  =  showOptionDialog;  // le bouton par défaut
			if( typeF!=JOptionPane.CLOSED_OPTION) ;// un bouton cliqué
			else typeF = BAS;  

			if(typeF== 0)typeF= HAUT;
			else if(typeF== 1)typeF= BAS;
			else if(typeF== 2)typeF= DROITE;
			else if(typeF== 3)typeF= GAUCHE;
					
		}
		
		
		

		if(e.getKeyCode()==10){  //ENTER
			
			this.addSelection(selection,typeF);
			
		}   
		
		
		
		
		
		
		System.out.println(e.getKeyCode());
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
