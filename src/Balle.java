import java.util.ArrayList;
import java.util.Iterator;


public class Balle extends Objet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient Iterator<Objet> iter;

	protected boolean directionX;
	protected boolean directionY;


	public Balle(int x,int y,boolean dx, boolean dy)
	{
		super(x,y);
		this.directionX = dx;
		this.directionY = dy;
	}

	public void checkAll(Snoopy snoop,ArrayList<Objet> liste){

		this.mouvementBalle();
		this.checkMur();
		this.checkObjet(liste);

	}

	private void mouvementBalle(){

		if(directionX) this.x++;
		else  this.x--;

		if(directionY) this.y++;
		else this.y--;

	}

	private void checkMur(){

		if(this.x>18) directionX = !directionX;
		if(this.x <1) directionX = !directionX;

		if(this.y <1)directionY = ! directionY;
		if(this.y >8)directionY = !directionY; 
	}



	private void checkObjet(ArrayList<Objet> liste){

		iter = liste.iterator();
		while(iter.hasNext()){

			Objet elem =iter.next();

			if(elem instanceof Bloc){

				if(elem.getX() == this.getX() && elem.getY() == this.getY())
				{
					this.directionX = ! this.directionX;
					this.directionY = ! this.directionY;
				}
			} 

			if(elem instanceof Snoopy){
				if(elem.getX() == this.getX() && elem.getY()== this.getY())
				{
					((Snoopy)elem).lostLife();

					this.directionX = ! this.directionX;
					this.directionY = ! this.directionY; 

				}

			}
		}
	}

	public boolean isDirectionX() {
		return directionX;
	}

	public void setDirectionX(boolean directionX) {
		this.directionX = directionX;
	}

	public boolean isDirectionY() {
		return directionY;
	}

	public void setDirectionY(boolean directionY) {
		this.directionY = directionY;
	}

	@Override
	public String toString() {
		return "Balle [directionX=" + directionX + ", directionY=" + directionY + ", toString()=" + super.toString()
		+ "]";
	}




}
