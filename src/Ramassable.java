
public class Ramassable extends Objet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int type ;
	public final int OISEAU = 1, PIEGE = 2;

	public Ramassable(){
		
		super();
	}
	
	public Ramassable(int x,int y,int type){
		super(x,y);
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}


	@Override
	public String toString() {
		return "Ramassable [type=" + type + ", toString()=" + super.toString() + "]";
	}
	
	
	
	
	

}
